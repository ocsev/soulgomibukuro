#!/bin/bash

echo 'copy words/ to public/'
cp -r words public

file=""

for entry in "public"/*
do
	echo "$entry"
	fn=$(basename -- "$entry")
	file+=${fn%.*}"\n"
done

echo -e $file >> public/index.html
